package SpringApplication.Services;

import SpringApplication.Models.Store;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class StoreServiceWithHystrix {
    @Autowired
    private RestTemplate restTemplate;
    private List<Store> cachedStores = new ArrayList<Store>();

    @HystrixCommand(fallbackMethod = "fallbackGetStores")
    public List<Store> getAllStores(){
        System.out.println("Send Localhost 8080 - API Request");
        List<Store> newStores = Arrays.asList(this.restTemplate.getForObject("http://localhost:8080/api/stores", Store[].class));
        System.out.println("Sent!");

        if(this.cachedStores.size() != newStores.size()){
            this.cachedStores = newStores;
        }

        return newStores;
    }

    @SuppressWarnings("unused")
    private List<Store> fallbackGetStores(){
        System.out.println("FALLBACK -> getting stores from cache");
        return this.cachedStores;
    }
}

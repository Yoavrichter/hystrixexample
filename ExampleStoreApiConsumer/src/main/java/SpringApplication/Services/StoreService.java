package SpringApplication.Services;

import SpringApplication.Models.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class StoreService {

    @Autowired
    private RestTemplate restTemplate;

    public List<Store> getAllStores(){
        System.out.println("Send Localhost 8080 - API Request");
        return Arrays.asList(this.restTemplate.getForObject("http://localhost:8080/api/stores", Store[].class));
    }
}

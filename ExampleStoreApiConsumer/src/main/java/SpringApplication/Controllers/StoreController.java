package SpringApplication.Controllers;

import SpringApplication.Models.Store;
import SpringApplication.Services.StoreService;
import SpringApplication.Services.StoreServiceWithHystrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/stores")
public class StoreController {
//    @Autowired
//    private StoreService storeService;
//
    @Autowired
    private StoreServiceWithHystrix storeService;

    @GetMapping("")
    public List<Store> getAllStores(){
        return this.storeService.getAllStores();
    }
}

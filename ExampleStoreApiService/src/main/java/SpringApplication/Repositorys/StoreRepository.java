package SpringApplication.Repositorys;

import SpringApplication.Models.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository<Store, Long> {
}

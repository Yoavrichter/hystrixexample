package SpringApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HystrixExampleUsersApplication {
    public static void main(String[] args) {
        SpringApplication.run(HystrixExampleUsersApplication.class, args);
    }
}

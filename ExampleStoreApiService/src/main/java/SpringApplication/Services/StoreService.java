package SpringApplication.Services;

import SpringApplication.Models.Store;
import SpringApplication.Repositorys.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreService {
    @Autowired
    private StoreRepository storeRepository;

    public List<Store> getAllStores(){
        return this.storeRepository.findAll();
    }
}

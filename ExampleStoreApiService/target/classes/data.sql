DROP TABLE IF EXISTS stores;

CREATE TABLE stores
(
    id         INTEGER PRIMARY KEY,
    store_name VARCHAR(100) NOT NULL,
    address    VARCHAR(100) NOT NULL
);

INSERT INTO stores (id, store_name, address) VALUES
                   (1, 'McDonald', '221B Baker Street'),
                   (2, 'Nike', '350 Fifth Avenue New York, NY 10118');
